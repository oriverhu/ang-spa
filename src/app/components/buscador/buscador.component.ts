import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from './../../services/heroes.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styles: [
  ]
})
export class BuscadorComponent implements OnInit {

  heroes:any[] = [];
  termino:string;

  constructor( private _activatedRoute: ActivatedRoute,
               private _heroesService:HeroesService
               ) { }

  ngOnInit(): void {

    this._activatedRoute.params.subscribe( params =>{  //obtener parametros de la url
          this.heroes  = this._heroesService.buscarHeroe(params.termino);
          this.termino = params.termino;
    })

  }

}
